var __interpretation_started_timestamp__
var pi = 3.141592653589793
var DEBUG = true
// ARTag код и разные вспомогательные функции
sign = function (n) {
	return n > 0 ? 1 : n = 0 ? 0 : -1
}
sqr = function (n) {
	return n * n
}
min = function (a, b) {
	return a < b ? a : b
}
max = function (a, b) {
	return a > b ? a : b
}
round = Math.round
abs = Math.abs
sqrt = Math.sqrt
sin = Math.sin
cos = Math.cos
atan2 = Math.atan2


var x_size = 160
var y_size = 120
var ar_cells = 8

function printArr(arr, delim) {
	delim = delim == undefined ? ',' : delim
	for (var i = 0; i < arr.length; i++) {
		print(arr[i])
	}
}

function listToMatrix(list, elementsPerSubArray) {
	var matrix = [],
		i, k
	for (i = 0, k = -1; i < list.length; i++) {
		if (i % elementsPerSubArray == 0) {
			k++
			matrix[k] = []
		}
		matrix[k].push(list[i])
	}
	return matrix
}

function matrixToList(matrix) {
	var list = []
	for (var i = 0; i < matrix.length; i++) {
		for (var j = 0; j < matrix[0].length; j++) {
			list.push(matrix[i][j])
		}
	}
	return list
}

function rotateMatrix90DegreesClockwise(array, threed) {
	var newArray
	if (threed) {
		newArray = cloneMovementMatrix(array)
	}
	else {
		newArray = array.slice()
	}
	newArray = newArray[0].map(function (col, i) {
		return newArray.map(function (row) {
			return row[i]
		})
	})
	for (var i = 0; i < newArray.length; i++) {
		newArray[i] = newArray[i].reverse()
	}
	return newArray
}

function cloneMovementMatrix(matrix) {
	var newMatrix = new Array(matrix.length)
	for (var i = 0; i < newMatrix.length; i++) {
		newMatrix[i] = new Array(matrix[i].length)
		for (var j = 0; j < newMatrix[i].length; j++) {
			newMatrix[i][j] = matrix[i][j].slice()
		}
	}
	return newMatrix
}

function intersect(x1, y1, x2, y2, x3, y3, x4, y4) { // Функция для нахождения пересечения двух отрезков

	// Проверяем, что никакие отрезки не имеют длину 0
	if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) {
		return false
	}

	denominator = ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1))

	// Проверяем, что отрезки не параллельны
	if (denominator === 0) {
		return false
	}

	var ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator
	var ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator

	// Проверяем, что пересечение входит в отрезки
	if (ua < 0 || ua > 1 || ub < 0 || ub > 1) {
		return false
	}

	// Возвращаем объект координат пересечения в точке [x, y]
	var x = x1 + ua * (x2 - x1)
	var y = y1 + ua * (y2 - y1)

	return [x, y]
}

function quadrilateralArea(x1, y1, x2, y2, x3, y3, x4, y4) { // Функция нахождения площади четырёхугольника
	return abs(x1 * y2 + x2 * y3 + x3 * y4 + x4 * y1 - x2 * y1 - x3 * y2 - x4 * y3 - x1 * y4) / 2
}

function rgbHexArrayToRGB24(rgbArray) {
	var rgb24Array = new Array(rgbArray.length)
	for (var i = 0; i < rgb24Array.length; i++) {
		rgb24Array[i] = parseInt(rgbArray[i], 16)
	}
	return rgb24Array
}

function rgb24ToRGB(rgb) { // Функция для преобразования формата rgb24 в формат RGB
	var R = (rgb & 0xFF0000) >> 16 // Сдвигаем маску красного цвета на 16 бит вправо
	var G = (rgb & 0x00FF00) >> 8 // Сдвигаем маску зелёного цвета на 8 бит вправо
	var B = (rgb & 0x0000FF) // Берём маску синего цвета
	return [R, G, B]
}

function rgb24ToGrey(matrix) { // Функция для преобразования матрицы rgb24 в матрицу серых оттенков
	var rgbMatrix = new Array(y_size)
	for (var i = 0; i < rgbMatrix.length; i++) {
		rgbMatrix[i] = new Array(x_size)
	}

	var greyMatrix = new Array(y_size)
	for (var i = 0; i < greyMatrix.length; i++) {
		greyMatrix[i] = new Array(x_size)
	}

	for (var i = 0; i < y_size; i++) {
		for (var j = 0; j < x_size; j++) {
			var RGB = rgb24ToRGB(matrix[i][j]) // Преобразуем rgb24 в RGB
			rgbMatrix[i][j] = RGB
			greyMatrix[i][j] = round(RGB[0] * 0.299 + RGB[1] * 0.587 + RGB[2] * 0.114) // Преобразовываем RGB цвет в оттенок серого посредством среднего арифметического каналов цветов и записываем в матрицу
		}
	}
	return greyMatrix
}

function greyToMask(greyMatrix) { // Функция для преобразования матрицы серых оттенков в матрицу-маску строго чёрно-белого формата (1 - чёрный, 0 - белый)
	const thresholdGrey = 255 / 8 // Константа границы серого. Если значение оттенка больше границы - то цвет белый, если меньше или равен - то чёрный
	var maskMatrix = new Array(y_size)
	for (var i = 0; i < maskMatrix.length; i++) {
		maskMatrix[i] = new Array(x_size)
	}
	for (var i = 0; i < y_size; i++) {
		for (var j = 0; j < x_size; j++) {
			maskMatrix[i][j] = greyMatrix[i][j] > thresholdGrey ? 1 : 0 // Сравниваем текущий оттенок с границей серого и записываем в матрицу
		}
	}
	return maskMatrix
}

function findCornersStraight(maskMatrix, delta) { // Функция для нахождения углов ARTag вертикально/горизонтально
	delta = delta || 0 // Сколько пикселей отрезать с каждой стороны изображения
	var ULcorner = [0, 0] // Переменная левого верхнего угла
	var URcorner = [0, 0] // Переменная правого верхнего угла
	var DLcorner = [0, 0] // Переменная нижнего левого угла
	var DRcorner = [0, 0] // Переменная нижнего правого угла

	// Нахождение левого верхнего угла
	var x = delta
	var y = delta
	while (maskMatrix[y][x] != 0) {
		//print(x + " " + y)
		if (x >= x_size - 1 - delta)
			y += 1, x = delta
		else
			x += 1
	}
	ULcorner = [x, y]

	// Нахождение правого верхнего угла
	var x = x_size - 1 - delta
	var y = delta
	while (maskMatrix[y][x] != 0) {
		if (y >= y_size - 1 - delta)
			x -= 1, y = delta
		else
			y += 1
	}
	URcorner = [x, y]

	// Нахождение нижнего левого угла
	var x = delta
	var y = delta
	while (maskMatrix[y][x] != 0) {
		if (y >= y_size - 1 - delta)
			x += 1, y = delta
		else
			y += 1
	}
	DLcorner = [x, y]

	// Нахождение нижнего правого угла
	var x = delta
	var y = y_size - 1 - delta
	while (maskMatrix[y][x] != 0) {
		if (x >= x_size - 1 - delta)
			y -= 1, x = delta
		else
			x += 1
	}
	DRcorner = [x, y]
	// Возвращение объекта углов ARTag
	return [ULcorner, URcorner, DLcorner, DRcorner]
}

function findCornersDiagonal(maskMatrix, delta) { // Функция для нахождения углов ARTag диагонально
	delta = delta || 0 // Сколько пикселей отрезать с каждой стороны изображения
	var ULcorner = [0, 0] // Переменная левого верхнего угла
	var URcorner = [0, 0] // Переменная правого верхнего угла
	var DLcorner = [0, 0] // Переменная нижнего левого угла
	var DRcorner = [0, 0] // Переменная нижнего правого угла

	// Нахождение левого верхнего угла
	var x = delta
	var y = delta
	var l = delta
	while (maskMatrix[y][x] != 0) {
		if (y >= y_size - 1 - delta)
			l += 1, y = delta, x = l
		if (x <= delta)
			l += 1, y = delta, x = l
		else
			y += 1, x -= 1
	}
	ULcorner = [x, y]
	if (DEBUG)
		print("ULcorner", ULcorner)

	// Нахождение правого верхнего угла
	var x = x_size - 1 - delta
	var y = delta
	var l = delta
	while (maskMatrix[y][x] != 0) {
		if (y >= y_size - 1 - delta) {
			l += 1, y = delta, x = x_size - 1 - delta - l
			continue
		}
		if (x >= x_size - 1 - delta)
			l += 1, y = delta, x = x_size - 1 - delta - l
		else
			y += 1, x += 1
	}
	URcorner = [x, y]
	if (DEBUG)
		print("URcorner", URcorner)

	// Нахождение левого нижнего угла
	var x = delta
	var y = y_size - 1 - delta
	var l = delta
	while (maskMatrix[y][x] != 0) {
		if (y <= 1 + delta) {
			l += 1, y = y_size - 1 - delta, x = l
			continue
		}
		if (x <= delta)
			l += 1, y = y_size - 1 - delta, x = l
		else
			y -= 1, x -= 1
	}
	DLcorner = [x, y]
	if (DEBUG)
		print("DLcorner", DLcorner)

	// Нахождение правого нижнего угла
	var x = x_size - 1 - delta
	var y = y_size - 1 - delta
	var l = delta
	while (maskMatrix[y][x] != 0) {
		if (y <= 1 + delta) {
			l += 1, y = y_size - 1 - delta, x = x_size - 1 - l - delta
			continue
		}
		if (x >= x_size - 1 - delta)
			l += 1, y = y_size - 1 - delta, x = x_size - 1 - delta - l
		else
			y -= 1, x += 1
	}
	DRcorner = [x, y]
	if (DEBUG)
		print("DRcorner", DRcorner)
	// Возвращение объекта углов ARTag
	return [ULcorner, URcorner, DLcorner, DRcorner]
}

function recARTag(sourceARTag) { // Функция для распознавания ARTag. На вход подаётся матрица маски
	var maskMatrix = sourceARTag.slice()
	for (var i = 0; i < y_size; i++) {
		for (var j = 0; j < 5; j++) {
			maskMatrix[i][j] = 1
		}
	}
	//printArr(maskMatrix)
	var corners1 = findCornersStraight(maskMatrix, 5)
	var corners = findCornersDiagonal(maskMatrix, 5)
	ULcorner = corners[0], URcorner = corners[1], DLcorner = corners[2], DRcorner = corners[3]
	if (DEBUG) {
		print(corners)
		brick.display().setPainterWidth(4)
		brick.display().drawLine(ULcorner[0], ULcorner[1], URcorner[0], URcorner[1])
		brick.display().drawLine(URcorner[0], URcorner[1], DRcorner[0], DRcorner[1])
		brick.display().drawLine(DRcorner[0], DRcorner[1], DLcorner[0], DLcorner[1])
		brick.display().drawLine(DLcorner[0], DLcorner[1], ULcorner[0], ULcorner[1])
		brick.display().addLabel("UL", ULcorner[0], ULcorner[1])
		brick.display().addLabel("UR", URcorner[0], URcorner[1])
		brick.display().addLabel("DL", DLcorner[0], DLcorner[1])
		brick.display().addLabel("DR", DRcorner[0], DRcorner[1])
		brick.display().redraw()
		script.wait(2000)
	}
	var centre = [(ULcorner[0] + URcorner[0] + DLcorner[0] + DRcorner[0]) / 4, (ULcorner[1] + URcorner[1] + DLcorner[1] + DRcorner[1]) / 4]

	var sideVectorUp = [URcorner[0] - ULcorner[0], URcorner[1] - ULcorner[1]]
	var lenUp = sqrt(sideVectorUp[0] * sideVectorUp[0] + sideVectorUp[1] * sideVectorUp[1])
	var unitVectorUp = [sideVectorUp[0] / lenUp, sideVectorUp[1] / lenUp]
	var angleUp = atan2(sideVectorUp[1], sideVectorUp[0])
	var onelenUp = lenUp / ar_cells
	var onevecUp = [unitVectorUp[0] * onelenUp, unitVectorUp[1] * onelenUp]

	var sideVectorLeft = [DLcorner[0] - ULcorner[0], DLcorner[1] - ULcorner[1]]
	var lenLeft = sqrt(sideVectorLeft[0] * sideVectorLeft[0] + sideVectorLeft[1] * sideVectorLeft[1])
	var unitVectorLeft = [sideVectorLeft[0] / lenLeft, sideVectorLeft[1] / lenLeft]
	var angleLeft = atan2(sideVectorLeft[1], sideVectorLeft[0])
	var onelenLeft = lenLeft / ar_cells
	var onevecLeft = [unitVectorLeft[0] * onelenLeft, unitVectorLeft[1] * onelenLeft]

	var sideVectorDown = [DRcorner[0] - DLcorner[0], DRcorner[1] - DLcorner[1]]
	var lenDown = sqrt(sideVectorDown[0] * sideVectorDown[0] + sideVectorDown[1] * sideVectorDown[1])
	var unitVectorDown = [sideVectorDown[0] / lenDown, sideVectorDown[1] / lenDown]
	var angleDown = atan2(sideVectorDown[1], sideVectorDown[0])
	var onelenDown = lenDown / ar_cells
	var onevecDown = [unitVectorDown[0] * onelenDown, unitVectorDown[1] * onelenDown]

	var sideVectorRight = [DRcorner[0] - URcorner[0], DRcorner[1] - URcorner[1]]
	var lenRight = sqrt(sideVectorRight[0] * sideVectorRight[0] + sideVectorRight[1] * sideVectorRight[1])
	var unitVectorRight = [sideVectorRight[0] / lenRight, sideVectorRight[1] / lenRight]
	var angleRight = atan2(sideVectorRight[1], sideVectorRight[0])
	var onelenRight = lenRight / ar_cells
	var onevecRight = [unitVectorRight[0] * onelenRight, unitVectorRight[1] * onelenRight]

	var ARTag = new Array(ar_cells - 2) // Так как ARTag имеет обводку чёрных квадратов (которые нужны только для его нахождения), нам не нужно записывать их в матрицу ARTag
	for (var i = 0; i < ARTag.length; i++) {
		ARTag[i] = new Array(ar_cells - 2)
	}

	for (var i = 1; i < ar_cells - 1; i++) { // Смотрим только на клетки внутри ARTag, не включая его края, т.к. они всегда чёрные
		for (var j = 1; j < ar_cells - 1; j++) {
			var cxU = ULcorner[0] + onevecUp[0] * j + onevecUp[0] / 2
			var cyU = ULcorner[1] + onevecUp[1] * j
			var cxD = ULcorner[0] + onevecDown[0] * j + onevecDown[0] / 2 + sideVectorLeft[0]
			var cyD = ULcorner[1] + onevecDown[1] * j + sideVectorLeft[1]
			var cxL = ULcorner[0] + onevecLeft[0] * i
			var cyL = ULcorner[1] + onevecLeft[1] * i + onevecLeft[1] / 2
			var cxR = ULcorner[0] + onevecRight[0] * i + sideVectorUp[0]
			var cyR = ULcorner[1] + onevecRight[1] * i + onevecRight[1] / 2 + sideVectorUp[1]
			var vec = intersect(cxU, cyU, cxD, cyD, cxL, cyL, cxR, cyR)
			if (DEBUG) {
				brick.display().clear()
				brick.display().drawLine(ULcorner[0], ULcorner[1], URcorner[0], URcorner[1])
				brick.display().drawLine(URcorner[0], URcorner[1], DRcorner[0], DRcorner[1])
				brick.display().drawLine(DRcorner[0], DRcorner[1], DLcorner[0], DLcorner[1])
				brick.display().drawLine(DLcorner[0], DLcorner[1], ULcorner[0], ULcorner[1])
				brick.display().addLabel("UL", ULcorner[0], ULcorner[1])
				brick.display().addLabel("UR", URcorner[0], URcorner[1])
				brick.display().addLabel("DL", DLcorner[0], DLcorner[1])
				brick.display().addLabel("DR", DRcorner[0], DRcorner[1])

				brick.display().addLabel("U", cxU, cyU[1])
				brick.display().addLabel("L", cxL, cyL)
				brick.display().addLabel("D", cxD, cyD)
				brick.display().addLabel("R", cxR, cyR)
				brick.display().drawLine(cxU, cyU, cxD, cyD)
				brick.display().drawLine(cxL, cyL, cxR, cyR)
				//brick.display().drawPoint(ULcorner[0] + vec[0], ULcorner[1] + vec[1])
				brick.display().redraw()
				//script.wait(500)
				//print(vec)
				//print(ULcorner[1] + round(vec[1]), " ", ULcorner[0] +round(vec[0]))
			}
			ARTag[i - 1][j - 1] = maskMatrix[round(vec[1])][round(vec[0])]
		}
	}
	if (DEBUG) {
		brick.display().redraw()
	}
	//script.wait(5000)
	if (DEBUG) {
		for (var i = 0; i < ARTag.length; i++) {
			print(ARTag[i])
		}
	}
	if (ARTag[0][0] == 1) {
		for (var i = 0; i < ARTag.length; i++) {
			ARTag[i].reverse()
		}
		ARTag.reverse()
	} else if (ARTag[ar_cells - 3][0] == 1) {
		var newArray = ARTag.reverse()
		for (var i = 0; i < newArray.length; i++) {
			for (var j = 0; j < i; j++) {
				var temp = newArray[i][j]
				newArray[i][j] = newArray[j][i]
				newArray[j][i] = temp
			}
		}
		for (var i = 0; i < newArray.length; i++) {
			newArray[i].reverse()
		}
		newArray.reverse()
		ARTag = newArray
	} else if (ARTag[0][ar_cells - 3] == 1) { // По часовой
		var newArray = ARTag.reverse()
		for (var i = 0; i < newArray.length; i++) {
			for (var j = 0; j < i; j++) {
				var temp = newArray[i][j]
				newArray[i][j] = newArray[j][i]
				newArray[j][i] = temp
			}
		}
		ARTag = newArray
	}
	if (DEBUG) {
		print("new")
		for (var i = 0; i < ARTag.length; i++) {
			print(ARTag[i])
		}
	}
	//brick.playTone(1000, 50)
	//brick.display().addLabel(numN + " ("+numX + ";" + numY + ")", 1, 1)
	//brick.display().redraw()

	return ARTag // Возвращаем ARTag
}

function parseARTag(ARTag) {
	for (var i = 0; i < ARTag.length; i++) {
		ARTag[i] = ARTag[i].map(function (a) {
			return String(a)
		})
	}
	var binary1 = [ARTag[0][3] + ARTag[1][0]]
	var binary2 = [ARTag[1][1] + ARTag[1][2]]
	var binary3 = [ARTag[1][4] + ARTag[1][5]]
	var binary4 = [ARTag[2][0] + ARTag[2][1]]
	var binary5 = [ARTag[2][2] + ARTag[2][3]]
	var binary6 = [ARTag[2][4] + ARTag[3][0]]
	var binary7 = [ARTag[3][1] + ARTag[3][2]]
	var binary8 = [ARTag[3][3] + ARTag[3][4]]
	var binary9 = [ARTag[3][5] + ARTag[4][0]]
	var binary10 = [ARTag[4][1] + ARTag[4][2]]
	var binary11 = [ARTag[4][3] + ARTag[4][4]]
	var binary12 = [ARTag[4][5] + ARTag[5][1]]
	var binary13 = [ARTag[5][2] + ARTag[5][3]]
	var num1 = parseInt(binary1, 2)
	var num2 = parseInt(binary2, 2)
	var num3 = parseInt(binary3, 2)
	var num4 = parseInt(binary4, 2)
	var num5 = parseInt(binary5, 2)
	var num6 = parseInt(binary6, 2)
	var num7 = parseInt(binary7, 2)
	var num8 = parseInt(binary8, 2)
	var num9 = parseInt(binary9, 2)
	var num10 = parseInt(binary10, 2)
	var num11 = parseInt(binary11, 2)
	var num12 = parseInt(binary12, 2)
	var num13 = parseInt(binary13, 2)

	return [num1, num2, num3, num4, num5, num6, num7, num8, num9, num10, num11, num12, num13]
}

function processARTagFromSource(source) {
	var HEX = true
	var newSource = source.slice()
	if (source.length == 1)
		newSource = newSource[0].trim().split(' ')
	if (HEX)
		newSource = rgbHexArrayToRGB24(newSource)
	else
		newSource = newSource.map(Number)
	var sourceMatrix = listToMatrix(newSource, x_size)
	var greyscale = rgb24ToGrey(sourceMatrix)
	var maskMatrix = greyToMask(greyscale)
	var data = recARTag(maskMatrix)
	var decodedData = parseARTag(data)
	if (DEBUG) {
		//print("decoded: ", decodedData)
		var maskMatrixSource = matrixToList(maskMatrix)
		maskMatrixSource = maskMatrixSource.map(function (a) {
			return a * 255
		})
		//print(maskMatrixSource)
		brick.display().show(maskMatrixSource, 160, 120, "grayscale8");
		brick.display().redraw()
		//script.wait(20000);
	}
	return decodedData
}
// ARTag заканчивается здесь
// Всё что связано с роботом
const dir = {
	UP: 0,
	RIGHT: 1,
	DOWN: 2,
	LEFT: 3
}
var trik = false
if (script.readAll("trik.txt").length) { // Проверяем на наличие файла trik.txt в папке со скриптом, так как на симуляторе этого файла не будет (Перед тем как загрузить программу на робота, нужно загрузить файл trik.txt в папку скриптов на роботе. Содержание файла не важно)
	trik = true
}
var robot = {
	d: trik ? 8.5 : 5.6,
	track: trik ? 17.3 : 17.5,
	cpr: trik ? 385 : 360,
	v: 85,
	curAngle: dir.UP,
	x: 0,
	y: 0,
	calibrationTime: trik ? 120000 : 5000,
	calibrationValues: trik ? [224, 216, -84, -104, -34, 4091] : undefined // [224, 216, -84, -104, -34, 4091]
}
var baseAngles = trik ? [0, 90, 180, -90] : [0, 90, 180, -90] // Порядок углов: спереди, справа, сзади, слева
var gyroAngles = [
	[baseAngles[0], baseAngles[1], baseAngles[2], baseAngles[3]],
	[baseAngles[3], baseAngles[0], baseAngles[1], baseAngles[2]],
	[baseAngles[2], baseAngles[3], baseAngles[0], baseAngles[1]],
	[baseAngles[1], baseAngles[2], baseAngles[3], baseAngles[0]]
]
const drift = trik ? 125 : 0 //200
var gyro = gyroAngles[robot.curAngle]
var readGyro = brick.gyroscope().read
mL = brick.motor('M4').setPower // Левый мотор
mR = brick.motor('M3').setPower // Правый мотор
eL = brick.encoder('E4').read // Левый энкодер
eR = brick.encoder('E3').read // Правый энкодер
sF = trik ? brick.sensor('A1').read : brick.sensor('D1').read // Сенсор спереди (ИК)
sL = trik ? brick.sensor('D2').read : brick.sensor('A1').read // Сенсор слева (УЗ)
sR = trik ? brick.sensor('D1').read : brick.sensor('A2').read // Сенсор справа (УЗ)

var eLeft = brick.encoder(E4)
var eRight = brick.encoder(E3)
var go = true


wait = script.wait

cell_size = trik ? 50 : 52.5 //В симуляторе - 52.5, в реальности - 50 (40 на НТИ)
maze_width = trik ? 4 : 8
maze_height = trik ? 2 : 8
var mazeMatrix = trik ? [
	[0, 0, 0, 0, 0, 0, 0, 0], 
	[0, 0, 1, 0, 0, 1, 0, 0], 
	[0, 1, 0, 0, 0, 0, 1, 0], 
	[0, 0, 0, 0, 0, 0, 0, 1], 
	[0, 0, 0, 0, 0, 1, 0, 0], 
	[0, 1, 0, 0, 1, 0, 0, 0], 
	[0, 0, 1, 0, 0, 0, 0, 1], 
	[0, 0, 0, 1, 0, 0, 1, 0], 
] : 
[
	[0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0]
]


function getCell(x, y) {
	return maze_width * y + x
}

function getCoord(cell) {
	return [cell % maze_width, Math.floor(cell / maze_width)]
}

function bfs(start, end) {
	var queue = [start]
	var P
	var path = []
	var visited = []
	for (var i = 0; i < mazeMatrix.length; i++) visited.push(false)
	while (queue.length) {
		P = queue.shift()
		path.push(P)
		if (P == end)
			break
		if (!visited[P]) {
			visited[P] = true
			for (var i = 0; i < mazeMatrix.length; i++) {
				if (mazeMatrix[P][i] == 1 && !visited[i]) queue.push(i)
			}
		}
	}
	path = path.reverse()
	var pathBack = [path.shift()]
	for (var i = 0; i < path.length; i++) {
		var lastP = pathBack.slice(-1)
		if (mazeMatrix[lastP][path[i]] == 1)
			pathBack.push(path[i])
	}
	path = pathBack.reverse()
	return path
}

function dfs(start, end) {
	var stack = [start]
	var P
	var path = [start]
	var Q
	var visited = []
	for (var i = 0; i < mazeMatrix.length; i++) visited.push(false)
	visited[start] = true
	while (stack.length) {
		P = stack[stack.length - 1]
		if (P == end)
			break
		hasNotVisitedList = []
		for (var i = 0; i < mazeMatrix.length; i++)
			if (mazeMatrix[P][i] == 1 && !visited[i]) {
				hasNotVisitedList.push(i)
			}
		if (hasNotVisitedList.length) {
			Q = hasNotVisitedList[0]
			visited[Q] = true
			stack.push(Q)
			path.push(Q)
		} else {
			stack.pop()
			path.pop()
		}
	}
	return path
}

function getPath(startCell, stopCell, startDir) {
	way = bfs(startCell, stopCell)
	var step
	var az = startDir
	var moves = []
	for (var i = 1; i < way.length; i++) {
		step = way[i] - way[i - 1]
		if (step == 4) {
			if (az == 0) {
				moves.push('L')
				moves.push('L')
			}
			if (az == 1) moves.push('R')
			if (az == 2) {}
			if (az == 3) moves.push('L')
			az = 2
		}
		if (step == 1) {
			if (az == 0) moves.push('R')
			if (az == 1) {}
			if (az == 2) moves.push('L')
			if (az == 3) {
				moves.push('L')
				moves.push('L')
			}
			az = 1
		}
		if (step == -4) {
			if (az == 0) {}
			if (az == 1) moves.push('L')
			if (az == 2) {
				moves.push('L')
				moves.push('L')
			}
			if (az == 3) moves.push('R')
			az = 0
		}
		if (step == -1) {
			if (az == 0) moves.push('L')
			if (az == 1) {
				moves.push('L')
				moves.push('L')
			}
			if (az == 2) moves.push('R')
			if (az == 3) {}
			az = 3
		}
		moves.push('F')
	}
	return moves
}

function goMoves(moves) {
	for (var i = 0; i < moves.length; i++) {
		displayCoords()
		switch (moves[i]) {
			case 'F':
				moveCells(1)
				break
			case 'L':
				placeTurnLeft()
				break
			case 'R':
				placeTurnRight()
				break
		}
	}
}

function cmToCPR(cm) { // Функция преобразования сантиметры в CPR энкодеров
	return (cm / (pi * robot.d)) * robot.cpr
}

var yawDrift = 0

function driftmeasure() { // Функция измерения дрейфа гироскопа. 662 миллиградусов на 5 секунд (?)
	print("yaw drift per 1 second: " + getYaw())
	brick.display().addLabel("yaw drift per 1 second: " + getYaw(), 1, 1) // Вывод поворота
	brick.display().redraw()
	yawDrift += getYaw()
	//script.exit()
}

function driftFix() { // Функция учёта дрейфа
	yawDrift += drift
}

function getYaw() { // Функция получения поворота робота в миллиградусах
	yawValue = readGyro()[6] - yawDrift
	if (yawValue > 180000) {
		yawValue = yawValue - 360000
	}
	if (yawValue < -180000) {
		yawValue = yawValue + 360000
	}
	return yawValue
}

function messages() {
	if (mailbox.hasMessages()) {
		var msg = mailbox.receive()
		brick.display().addLabel(msg, 10, 10)
		brick.display().redraw()
		script.wait(100)
	}
}

function motors(vl, vr) {
	mL(vl == undefined ? robot.v : vl)
	mR(vr == undefined ? robot.v : vr)
}

// Базовые функции. Лучше не использовать в основном коде

function turnEnc(angle) { // Функция поворота по энкодеру
	var sgn = sign(angle)
	var eN = sgn == 1 ? eL : eR
	var path = eN() + (robot.track * abs(angle)) / (robot.d * 360) * robot.cpr
	motors(40 * sgn, -40 * sgn)
	while (eN() < path) {
		wait(10)
	}
	motors(0, 0)
}

function turnGyro(angle) { // Функция поворота по гироскопу
	if (abs(angle) < 200) angle *= 1000
	if (angle > 180000) angle = (angle - (angle - 180000) * 2) * -1
	var currentYaw = getYaw()
	var sgn = 1
	var leftBoundary = currentYaw - 180000
	if (angle > leftBoundary && angle < currentYaw)
		sgn = -1
	else if (leftBoundary < -180000) {
		leftBoundary = abs(leftBoundary) - (abs(leftBoundary) - 180000) * 2
		if (angle > leftBoundary) sgn = -1
	}
	motors(73 * sgn, -73 * sgn)
	while (abs(angle - getYaw()) > 4000)
		wait(1)
	motors(0, 0)
}

function moveBackwards(cm) {
	print('Moving back for ' + cm + ' cm')
	var path = eL() - cmToCPR(cm)
	motors(-robot.v, -robot.v)
	while (eL() > path) {
		wait(10)
	}
	motors(0, 0)
}

function moveStraight(cm) {
	var sgn = sign(cm)
	print('Moving for ' + cm + ' cm')
	var path = eL() + cmToCPR(abs(cm)) * sgn
	motors(robot.v * sgn, robot.v * sgn)
	if (sgn == 1)
		while (eL() < path) {
			wait(10)
		}
	else
		while (eL() > path) {
			wait(10)
		}
	motors(0, 0)
}

function encoderError(encLst, encRst) { // Функция возвращения ошибки энкодера
	return (eL() - encLst) - (eR() - encRst)
}

function gyroscopeError(gyroStraight) { // Функция возвращения ошибки гироскопа
	var gyroError
	if (gyroStraight != baseAngles[2]) {
		gyroError = getYaw() / 1000 - gyroStraight
	} else {
		gyroError = (abs(getYaw()) / 1000 - gyroStraight) * sign(getYaw())
	}
	return gyroError
}

function distanceError(distCentre, distLimit) { // Функция возвращения ошибки расстояния
	var distError = sL() - sR()
	if (sL() > distLimit && sR() > distLimit) {
		distError = 0
	} else if (sL() > distLimit) {
		distError = distCentre - sR()
	} else {
		distError = sL() - distCentre
	}
	return distError
}

function moveSmooth(cm, v) { // Функция для плавного передвижения
	v = v == undefined ? robot.v : v
	var sgn = sign(cm)
	cm = abs(cm)
	var pathStart = eL()
	var path = pathStart + cmToCPR(cm) * sgn
	var v0 = 50,
		vM = v0
	var startStop = cmToCPR(cm) / 4
	var dV = (v - v0) / 10
	var encLst = eL()
	var encRst = eR()
	var gyroStraight = gyro[robot.curAngle]
	var distCentre = trik ? 15 : 15
	var distLimit = trik ? 30 : 30
	var distStop = trik ? 11 : 25
	print("gyroStraight" + gyroStraight)
	// Коэффиценты
	var encd_kP = trik ? 0.0 : 0.0
	var encd_kD = trik ? 0.0 : 0.0
	var gyro_kP = trik ? 0.0 : 3.5 // 3.5
	var gyro_kD = trik ? 0.0 : 0.0
	var dist_kP = trik ? 2.5 : 0.0
	var dist_kD = trik ? 0.5 : 0.0
	var kP_array = [encd_kP, gyro_kP, dist_kP] //0 - энкодер, 1 - гироскоп, 2 - датчик расстояния
	var kD_array = [encd_kD, gyro_kD, dist_kD] //0 - энкодер, 1 - гироскоп, 2 - датчик расстояния
	// Переменные ошибок
	var gyroError = 0
	var encdError = 0
	var distError = 0
	var errorArray = [0, 0, 0]
	var lastErrorArray = [0, 0, 0]
	var deltaErrorArray = [0, 0, 0]
	// Переменные контролей
	var encdControl = 0
	var gyroControl = 0
	var distControl = 0
	var controlArray = [0, 0, 0]
	var control = 0
	if (sgn == 1)
		while (eL() < path && sF() > distStop) {
			if (eL() < pathStart + startStop) vM += dV * 2
			if (eL() > pathStart + startStop * 3) vM -= dV / 5
			vM = Math.min(Math.max(v0, vM), v)
			// Вычисление ошибки энкодера
			encdError = encoderError(encLst, encRst)
			errorArray[0] = encdError
			// Вычисления ошибки гироскопа
			gyroError = gyroscopeError(gyroStraight)
			errorArray[1] = gyroError
			// Вычисление ошибки расстояния
			distError = distanceError(distCentre, distLimit)
			errorArray[2] = distError
			// Вычисление изменений ошибок
			for (var i = 0; i < errorArray.length; i++) { // Цикл для записи разности в значениях ошибок и перезаписи значения последних значений ошибок
				deltaErrorArray[i] = lastErrorArray[i] - errorArray[i]
				lastErrorArray[i] = errorArray[i]
			}
			// Суммируем все ошибки умноженные на их коэффиценты
			control = 0
			for (var i = 0; i < errorArray.length; i++) {
				controlArray[i] = errorArray[i] * kP_array[i] + deltaErrorArray[i] * kD_array[i]
				control += controlArray[i]
			}

			//print("gyrocontrol" + gyroControl)
			//print("encdcontrol" + encdControl)
			//print("yaw " + getYaw() / 1000)
			//brick.display().addLabel(control, 1, 1)
			//control = encdControl + gyroControl + distControl
			//brick.display().addLabel("motor speeds: " + (vM - control) + " " + (vM + control) , 1, 20)
			//brick.display().redraw()
			//brick.display().addLabel(getYaw() / 1000, 1, 1)
			//brick.display().redraw()
			motors(Math.min(vM - control, 100), Math.min(vM + control, 100))
			wait(35)
		}
	else if (sgn == -1)
		while (eL() > path) {
			if (eL() > pathStart - startStop) vM += dV
			if (eL() < pathStart - startStop * 3) vM -= dV
			vM = Math.max(v0, vM)
			motors(-vM, -vM)
			wait(35)
		}
	motors(0, 0)
}

function displayCoords() {
	brick.display().addLabel("(" + robot.x + ";" + robot.y + ")" + robot.curAngle, 1, 1) // Вывод ответа
	brick.display().redraw()
}

function displayYaw() {
	brick.display().addLabel(getYaw() / 1000, 1, 1) // Вывод поворота
	brick.display().redraw()
}
// Основные функции кода

function placeTurnTo(azimuth) {
	robot.curAngle = azimuth
	//if (!trik) moveStraight(robot.track / 2)
	turnGyro(gyro[robot.curAngle])
	//if (!trik) moveStraight(-robot.track / 2)
}

function placeTurnRight() { // Функция поворота на 90 градусов направо
	robot.curAngle = robot.curAngle + 1 > 3 ? 0 : robot.curAngle + 1
	if (!trik) moveStraight(robot.track / 2)
	turnGyro(gyro[robot.curAngle])
	if (!trik) moveStraight(-robot.track / 2)
}

function placeTurnLeft() { // Функция поворота на 90 градусов налево
	robot.curAngle = robot.curAngle - 1 < 0 ? 3 : robot.curAngle - 1
	if (!trik) moveStraight(robot.track / 2)
	turnGyro(gyro[robot.curAngle])
	if (!trik) moveStraight(-robot.track / 2)
}

function moveCells(cells) {
	moveSmooth(cells * cell_size)
	switch (robot.curAngle) {
		case dir.UP:
			robot.y -= 1 * cells
			break
		case dir.RIGHT:
			robot.x += 1 * cells
			break
		case dir.DOWN:
			robot.y += 1 * cells
			break
		case dir.LEFT:
			robot.x -= 1 * cells
			break
	}
	placeTurnTo(robot.curAngle)
}

// Функции для интеллектуальной навигации

function getFreeDirections(distFree, leftSensorFunc, forwardSensorFunc, rightSensorFunc, backSensorFunc) {
	var freeDirArray = new Array(4)
	freeDirArray[dir.LEFT] = leftSensorFunc != undefined ? leftSensorFunc() : -1
	freeDirArray[dir.UP] = forwardSensorFunc != undefined ? forwardSensorFunc() : -1
	freeDirArray[dir.RIGHT] = rightSensorFunc != undefined ? rightSensorFunc() : -1
	freeDirArray[dir.DOWN] = backSensorFunc != undefined ? backSensorFunc() : -1
	for (var i = 0; i < 4; i++) {
		if (freeDirArray[i] != -1) {
			freeDirArray[i] = freeDirArray[i] >= distFree ? 1 : 0
		}
	}
	return freeDirArray
}

function mazeMove(leftHand) { 
	leftHand = leftHand == undefined ? true : leftHand // Если не указано следовать по левой руке или нет, то предполагаем что надо следовать по левой руке
	var freeDirections = getFreeDirections(cell_size, sL, sF, sR)
	var isFreeForward = freeDirections[dir.UP]
	var isFreeLeft = freeDirections[dir.LEFT]
	var isFreeRight = freeDirections[dir.RIGHT]
	if (leftHand && isFreeLeft || !leftHand && isFreeRight) {
		if (leftHand)
			placeTurnLeft()
		else
			placeTurnRight()
		moveCells(1)
		return true
	}
	else if (isFreeForward){
		moveCells(1)
		return true
	}
	else if (leftHand) {
		placeTurnRight()
		return false
	}
	else {
		placeTurnLeft()
		return false
	}
}

//Функции для локализации
function makeMovementMatrix(mazeHeight, mazeWidth, mazeMatrix) { // Делает матрицу направлений, в которые можно переместиться. каждая клетка - массив из четырёх элементов, где 1 - можно переместиться и 0 нельзя переместиться
	var mazeMovementMatrix = new Array(mazeHeight)
	for (var i = 0; i < mazeHeight; i++) {
		mazeMovementMatrix[i] = new Array(mazeWidth)
		for (var j = 0; j < mazeWidth; j++){
			mazeMovementMatrix[i][j] = [0, 0, 0, 0]
		}
	}
	for (var i = 0; i < mazeMatrix.length; i++) {
		for (var j = 0; j < mazeMatrix.length; j++) {
			if (mazeMatrix[i][j] == 1) {
				var coords = getCoord(i)
				print("i: ", i)
				print(coords)
				switch (j - i) { // Берём разницу клеток
					case -1: // Можно ли идти налево
						mazeMovementMatrix[coords[1]][coords[0]][dir.LEFT] = 1 
						break
					case -maze_width: // Можно ли идти вверх
						mazeMovementMatrix[coords[1]][coords[0]][dir.UP] = 1 
						break
					case 1: // Можно ли идти вправо
						mazeMovementMatrix[coords[1]][coords[0]][dir.RIGHT] = 1 
						break
					case maze_width: // Можно ли идти вниз
						mazeMovementMatrix[coords[1]][coords[0]][dir.DOWN] = 1 
						break
				}
			}
		}
	}
	return mazeMovementMatrix
}

function readingsRelativeToGlobal(sensorReadings, relativeAngle) { // Функция для преобразования показаний датчиков расстояния из показаний относительно робота в показания относительно лабиринта
	var globalSensorReadings = new Array(4)
	switch (relativeAngle) {
		case dir.UP:
			globalSensorReadings[dir.UP] = sensorReadings[dir.UP]
			globalSensorReadings[dir.RIGHT] = sensorReadings[dir.RIGHT]
			globalSensorReadings[dir.DOWN] = sensorReadings[dir.DOWN]
			globalSensorReadings[dir.LEFT] = sensorReadings[dir.LEFT]
			break
		case dir.RIGHT:
			globalSensorReadings[dir.UP] = sensorReadings[dir.LEFT]
			globalSensorReadings[dir.RIGHT] = sensorReadings[dir.UP]
			globalSensorReadings[dir.DOWN] = sensorReadings[dir.RIGHT]
			globalSensorReadings[dir.LEFT] = sensorReadings[dir.DOWN]
			break
		case dir.DOWN:
			globalSensorReadings[dir.UP] = sensorReadings[dir.DOWN]
			globalSensorReadings[dir.RIGHT] = sensorReadings[dir.LEFT]
			globalSensorReadings[dir.DOWN] = sensorReadings[dir.UP]
			globalSensorReadings[dir.LEFT] = sensorReadings[dir.RIGHT]
			break
		case dir.LEFT:
			globalSensorReadings[dir.UP] = sensorReadings[dir.RIGHT]
			globalSensorReadings[dir.RIGHT] = sensorReadings[dir.DOWN]
			globalSensorReadings[dir.DOWN] = sensorReadings[dir.LEFT]
			globalSensorReadings[dir.LEFT] = sensorReadings[dir.UP]
			break
	}
	return globalSensorReadings
}

function getUpdatedRobotMazeMatrix(robotMazeMatrix) {
	var newRobotMazeMatrix = new Array(robotMazeMatrix.length)
	newRobotMazeMatrix = cloneMovementMatrix(robotMazeMatrix)
	if (robot.x < 0) {
		for (var i = 0; i < newRobotMazeMatrix.length; i++) {
			newRobotMazeMatrix[i].unshift([-1, -1, -1, -1])
			newRobotMazeMatrix[i].pop()
		}
		robot.x = 0
	}
	if (robot.y < 0) {
		newRobotMazeMatrix.unshift(new Array(newRobotMazeMatrix[0].length))
		for (var i = 0; i < newRobotMazeMatrix[0].length; i++) {
			newRobotMazeMatrix[0][i] = [-1, -1, -1, -1]
		}
		newRobotMazeMatrix.pop()
		robot.y = 0
	}
	var curReadings = getFreeDirections(cell_size, sL, sF, sR)
	var globalReadings = readingsRelativeToGlobal(curReadings, robot.curAngle)
	for (var i = 0; i < 4; i++) {
		newRobotMazeMatrix[robot.y][robot.x][i] = globalReadings[i] != -1 ? globalReadings[i] : newRobotMazeMatrix[robot.y][robot.x][i]
	}
	return newRobotMazeMatrix
}

function getCroppedRobotMazeMatrix(robotMazeMatrix) {
	var newRobotMazeMatrix = new Array(robotMazeMatrix.length)
	newRobotMazeMatrix = cloneMovementMatrix(robotMazeMatrix)
	//print("base matrix: ", newRobotMazeMatrix.join('\n'))
	for (var i = newRobotMazeMatrix.length - 1; i >= 0; i--) { // Проверяем на пустые строки
		var removeRow = true
		for (var j = 0; j < newRobotMazeMatrix[i].length; j++) {
			//print("checking i ", i, " j ", j)
			//print("c: ", newRobotMazeMatrix[i][j])
			if (newRobotMazeMatrix[i][j].join('') != [-1, -1, -1, -1].join('')) {
				//print("not right: ", newRobotMazeMatrix[i][j])
				removeRow = false
				break
			}
		}
		if (removeRow) {
			//print("removing row")
			newRobotMazeMatrix.splice(i, 1)
		}
	}
	for (var i = newRobotMazeMatrix[0].length - 1; i >= 0; i--) { // Проверяем на пустые столбцы
		var removeColumn = true
		for (var j = 0; j < newRobotMazeMatrix.length; j++) {
			if (newRobotMazeMatrix[j][i].join('') != [-1, -1, -1, -1].join('')) {
				removeColumn = false
				break
			}
		}
		if (removeColumn) {
			for (var j = 0; j < newRobotMazeMatrix.length; j++) {
				newRobotMazeMatrix[j].splice(i, 1)
			}
		}
	}
	//print("cropped: ", newRobotMazeMatrix.join('\n'))
	return newRobotMazeMatrix
}

function getAllValidLocations(mazeMovementMatrix, robotMazeMatrix, currentX, currentY, currentAzimuth) {
	var newRobotMazeMatrix = new Array(robotMazeMatrix.length)
	newRobotMazeMatrix = cloneMovementMatrix(robotMazeMatrix)
	newRobotMazeMatrix[currentY][currentX].push(currentAzimuth)
	var resolutionMazeMovementMatrix = [mazeMovementMatrix.length, mazeMovementMatrix[0].length]
	var resolutionNewRobotMazeMatrix = [newRobotMazeMatrix.length, newRobotMazeMatrix[0].length]
	var currentMatches = []
	print("loc matrix: ", newRobotMazeMatrix)
	print("base matrix: ", robotMazeMatrix)
	
	for (var turn = 0; turn < 4; turn++) {
		for (var offsetY = 0; offsetY < resolutionMazeMovementMatrix[0] - resolutionNewRobotMazeMatrix[0] + 1; offsetY++) {
			for (var offsetX = 0; offsetX < resolutionMazeMovementMatrix[1] - resolutionNewRobotMazeMatrix[1] + 1; offsetX++) {
				var matching = true
				for (var row = 0; row < resolutionNewRobotMazeMatrix[0]; row++) { // Поэлементное сравнение
					for (var column = 0; column < resolutionNewRobotMazeMatrix[1]; column++) {
						for (var elemIndex = 0; elemIndex < 4; elemIndex++) {
							if (newRobotMazeMatrix[row][column][elemIndex] == -1 || mazeMovementMatrix[offsetY + row][offsetX + column][elemIndex] == newRobotMazeMatrix[row][column][elemIndex])
							{
								// Тут какой-то код, если правильно сравнивает элемент
							}
							else {
								matching = false // Не подходит, перестаём сравнивать на текущих offsetX offsetY
								break
							}
						}
						if (!matching) {
							break
						}
					}
					if (!matching) {
						break
					}
				}
				if (matching) {
					var actualOffsetX = offsetX
					var actualOffsetY = offsetY + resolutionNewRobotMazeMatrix[0] - 1
					currentMatches.push([actualOffsetX + currentX, actualOffsetY + currentY - resolutionNewRobotMazeMatrix[0] + 1, currentAzimuth].slice())
				}
			}
		}
		// Закончили поиск, поворачиваем
		newRobotMazeMatrix = rotateMatrix90DegreesClockwise(newRobotMazeMatrix, true)
		resolutionNewRobotMazeMatrix = [newRobotMazeMatrix.length, newRobotMazeMatrix[0].length]
		for (var row = 0; row < resolutionNewRobotMazeMatrix[0]; row++) {
			for (var column = 0; column < resolutionNewRobotMazeMatrix[1]; column++) {
				var baseElems = newRobotMazeMatrix[row][column].slice()
				newRobotMazeMatrix[row][column][dir.UP] = baseElems[dir.LEFT]
				newRobotMazeMatrix[row][column][dir.RIGHT] = baseElems[dir.UP]
				newRobotMazeMatrix[row][column][dir.DOWN] = baseElems[dir.RIGHT]
				newRobotMazeMatrix[row][column][dir.LEFT] = baseElems[dir.DOWN]
				if (baseElems.length == 5) {
					switch (baseElems[4]) {
						case dir.UP:
							newRobotMazeMatrix[row][column][4] = dir.RIGHT
							break
						case dir.RIGHT:
							newRobotMazeMatrix[row][column][4] = dir.DOWN
							break
						case dir.DOWN:
							newRobotMazeMatrix[row][column][4] = dir.LEFT
							break
						case dir.LEFT:
							newRobotMazeMatrix[row][column][4] = dir.UP
							break
					}
					currentX = column
					currentY = row
					currentAzimuth = newRobotMazeMatrix[row][column][4]
				}
			}
		}
	}
	return currentMatches
}

function localize(mazeHeight, mazeWidth, mazeMatrix) { // Функция локализации робота, возвращает настоящие координаты робота относительно левого верхнего угла и поворот
	var mazeMovementMatrix = makeMovementMatrix(mazeHeight, mazeWidth, mazeMatrix)
	var actX = 0 // Переменная, которая хранит настоящую координату X (абсцисса) робота
	var actY = 0 // Переменная, которая хранит настоящую координату Y (ордината) робота
	var actDir = 0 // Переменнаая, которая хранит настоящий поворот (азимут) робота
	var leftHand = true

	var robotMazeMatrix = new Array(mazeHeight)
	for (var i = 0; i < mazeHeight; i++) {
		robotMazeMatrix[i] = new Array(mazeWidth)
		for (var j = 0; j < mazeWidth; j++) {
			robotMazeMatrix[i][j] = [-1, -1, -1, -1]
		}
	}
	var localized = false
	while (true && !localized) {
		robotMazeMatrix = getUpdatedRobotMazeMatrix(robotMazeMatrix)
		//print("base mat: ", robotMazeMatrix.join('\n'))
		var croppedRobotMazeMatrix = getCroppedRobotMazeMatrix(robotMazeMatrix)
		print("cropped: ", croppedRobotMazeMatrix.join('\n'))
		var currentMatches = getAllValidLocations(mazeMovementMatrix, croppedRobotMazeMatrix, robot.x, robot.y, robot.curAngle)
		//var currentMatches = "22"
		print("matches: ", currentMatches.length)
		if (currentMatches.length == 1) {
			actX = currentMatches[0][0]
			actY = currentMatches[0][1]
			actDir = currentMatches[0][2]
			localized = true
			break
		}
		else {
			mazeMove(leftHand)
			//script.wait(2000)
		}
	}
	return [actX, actY, actDir]
}
// Основная программа. Функция должна быть в конце файла

var main = function () {
	__interpretation_started_timestamp__ = Date.now()
	eLeft.reset()
	eRight.reset()
	var source = script.readAll("test_0.txt") // Код для проверки распознавания ARTag
	if (source.length) {
		var decodedData = processARTagFromSource(source)
		if (DEBUG) {
			print(decodedData)
		}
	}
	if (trik) { //TRIK
		print("Running on TRIK")
		//mailbox.connect("10.240.23.12") Ip: 10.240.23.12 in Innopolis
		//print(mailbox.myHullNumber())
		//var pager = script.timer(20)
		//pager.timeout.connect(messages)
	} else print("Running on simulator")
	if (robot.calibrationValues) brick.gyroscope().setCalibrationValues(robot.calibrationValues)
	else {
		brick.gyroscope().calibrate(robot.calibrationTime) // 5000мс в симуляторе, 14000мс на реальном роботе
		wait(robot.calibrationTime + 1000)
		print(brick.gyroscope().getCalibrationValues())
		//return
	}
	if (trik) {
		//var driftTimer = script.timer(1000)
		//driftTimer.timeout.connect(driftFix)
		//var displayTimer = script.timer(100)
		//displayTimer.timeout.connect(displayYaw)
	}	
	while (true && !go){
	    brick.display().addLabel(getYaw() / 1000, 1, 1) // Вывод поворота
	    brick.display().redraw()
	    script.wait(10)
	}
	/*moveCells(1)
	placeTurnLeft()
	moveCells(1)
	placeTurnRight()
	moveCells(1)
	placeTurnRight()
	moveCells(1)
	placeTurnLeft()
	moveCells(1)
	placeTurnLeft()
	moveCells(1)
	return*/
	var coords = localize(maze_height, maze_width, mazeMatrix)
	print(coords)
	robot.x = coords[0]
	robot.y = coords[1]
	switch (robot.curAngle - coords[2]){ // Смотрим на разницу истинного угла и вычисленного угла, и меняем углы гироскопа в зависимости от этого
		case 0:
			gyro = gyroAngles[0]
			break
		case 3:
		case -1:
			gyro = gyroAngles[1]
			break
		case 2:
		case -2:
			gyro = gyroAngles[2]
			break
		case 1:
		case -3:
			gyro = gyroAngles[3]
			break
	}
	robot.curAngle = coords[2]
	print("x: ", robot.x)
	print("y: ", robot.y)
	print("dir: ", robot.curAngle)
	print("gyro: ", gyro)
	displayCoords()
	script.wait(1000)
	while (true) {
		mazeMove(true)
	}
	return
	var cellStart = 1
	var cellEnd = 8
	//script.wait(10000)
	print(bfs(cellStart, cellEnd))
	print(dfs(cellStart, cellEnd))
	var robotPath = getPath(cellStart, cellEnd, robot.curAngle)
	print(robotPath)

	goMoves(robotPath)
	var photo = getPhoto()
	//script.writeToFile("photo.txt",photo)
	//brick.display().show(photo, 160, 120, 'rgb32')
	var nums = recARTag(photo, x_size, y_size, 0)
	//script.wait(2500)
	print(nums)
	brick.display().redraw()
	brick.display().addLabel("(" + nums[0] + ";" + nums[1] + ")" + nums[2], 1, 1)
	brick.display().redraw()
	robotPath = getPath(cellEnd, getCell(nums[0], nums[1]), robot.curAngle)
	print(robotPath)
	brick.display().addLabel(robotPath, 1, 1)
	brick.display().redraw()
	script.wait(5000)
	goMoves(robotPath)
	script.wait(5000)
	return
}
main() //Вызов основной программы. ДОЛЖНО БЫТЬ ПОСЛЕДНЕЙ СТРОЧКОЙ КОДА В ПРОГРАММЕ!